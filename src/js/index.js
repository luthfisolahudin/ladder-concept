(() => {
    const ladders = document.querySelectorAll('.ladder')

    const computedStylePixel = (element, property) => {
        return Number(
            getComputedStyle(element)
            .getPropertyValue(property)
            .replace('px', '')
        )
    }
    const plotSize = computedStylePixel(document.querySelector('.board'), '--plot-size')

    ladders.forEach((ladder, idx) => {
        const wrapper = ladder.closest('.ladder-wrapper')
        const visual = ladder.querySelector('.ladder-visual')

        const config = {
            plotStart: {
                x: Number(ladder.dataset.plotStartX) - 1,
                y: Number(ladder.dataset.plotStartY) - 1,
            },
            plotEnd: {
                x: Number(ladder.dataset.plotEndX),
                y: Number(ladder.dataset.plotEndY),
            },
        }
        const [minX, maxX] = [config.plotStart.x, config.plotEnd.x].sort()
        const [minY, maxY] = [config.plotStart.y, config.plotEnd.y].sort()

        wrapper.style.setProperty('left', `calc(var(--plot-size) * ${minX})`)
        wrapper.style.setProperty('bottom', `calc(var(--plot-size) * ${minY})`)

        ladder.style.setProperty('width', `calc(var(--plot-size) * ${maxX - minX})`)
        ladder.style.setProperty('height', `calc(var(--plot-size) * ${maxY - minY})`)

        const [x, y] = [
            computedStylePixel(ladder, 'width') - plotSize,
            computedStylePixel(ladder, 'height') - plotSize,
        ]

        const hypotenusa = Math.hypot(x, y)
        let angle = Math.asin(x / hypotenusa) * 180 / Math.PI

        if (config.plotStart.x > config.plotEnd.x) {
            angle *= -1
        }

        const visualLength = hypotenusa - plotSize * .25

        visual.style.setProperty('height', `${visualLength}px`)
        visual.style.setProperty('rotate', `${angle}deg`)
    })
})()
