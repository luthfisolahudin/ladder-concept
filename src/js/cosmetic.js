(() => {
    const ladders = document.querySelectorAll('.ladder')
    const plotX = Number(getComputedStyle(document.querySelector('.board')).getPropertyValue('--plot-x'))

    const computedStylePixel = (element, property) => Number(getComputedStyle(element).getPropertyValue(property).replace('px', ''))
    const plotCoordToNumber = (x, y) => (y - 1) * plotX + x

    ladders.forEach((ladder) => {
        const visual = ladder.querySelector('.ladder-visual')
        const visualLength = computedStylePixel(visual, 'height')

        const plotStartNumber = plotCoordToNumber(
            Number(ladder.dataset.plotStartX),
            Number(ladder.dataset.plotStartY),
        )
        const plotEndNumber = plotCoordToNumber(
            Number(ladder.dataset.plotEndX),
            Number(ladder.dataset.plotEndY),
        )

        visual.style.setProperty('line-height', `${visualLength}px`)
        visual.textContent = plotEndNumber - plotStartNumber

        ladder.addEventListener(
            'mouseenter',
            ({ target }) => target.classList.add('hover')
        )
        ladder.addEventListener(
            'mouseleave',
            ({ target }) => setTimeout(() => target.classList.remove('hover'), 500)
        )
    })
})()
